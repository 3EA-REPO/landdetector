
/**
 * Compute scales
 */
#ifdef scaleEnable
void processScale(){
  //Serial.println("Scale");

  if ( armed == true && taredone == false ){
    taredone = true;
    //unsigned long time = millis();
    scales.tare(5, 100000);// calibrate the scales each time when the copter arms
    //Serial.println(millis()-time);
  }
  if ( armed == false ){
    taredone=false;
  }                                     //set the state to not calibrated if the copter disarms

  //3412 -> 1234
  //!! Die Skalierung benötigt noch eine Integration in die Gui
  // da jedes Gestell seine eigene kalibrierung benötigt 
  scales.read(results);
  weight  = 100;//(results[0] / 10000) * 100.0f/currentConfig.FREETHREE ; // 3  | Messung -- 90
  weight2 = 100;//(results[1] / 10000) * 100.0f/currentConfig.FREEFOUR; // 4  | Messung -- 40
  weight3 = (results[0] / 10000) * 100.0f/currentConfig.FREEONE; // 1  | Messung -- 71
  weight4 = (results[1] / 10000) * 100.0f/currentConfig.FREETWO; // 2  | Messung -- 97
  //weightMean = weight; //should maybe filtered but it consumes time. HX711 datarate is limeted

  //

  //
  #ifdef debugScale                       
  Serial.print("HL "); Serial.print (weight); Serial.print("\tHR "); Serial.println(weight4); //Prototype DMS Landinggear
  Serial.print("VL "); Serial.print(weight2); Serial.print("\tVR "); Serial.println(weight3);
  #endif
  
}


#endif
