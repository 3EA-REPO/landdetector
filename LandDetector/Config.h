
//Define the default Parameter for Land detection
#define minActivationAlt 30.0f //in cm minimum alt to detect copter takeoff
#define minGearToGroundDistance 10.0f // in cm, under this level the landdetection can really disarm motors
#define minWeight -30        //minimum weight value to identify a touchdown at start it will tare to zero in flight aprox 40
#define minAcc -1500 //Acceleration G*1000 Impact accel when landing.
#define storeAccPeakTime 300   // Time to store an ACC Peak for land detection 
