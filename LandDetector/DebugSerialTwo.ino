/**
 * Wlan Debug Message on Serial2
 * */ 
 
//#ifdef debugSerialTwo

void debugWlan(){
    static unsigned long timettt =0;
    if(millis() > 1000 + timettt){
        //Serial2.println("hallo");
        timettt = millis();
    }
    
    {
    if(armed||logDelay==true){
    
    logDelay=true;
    if (armed==false){
      
      logDelayTimer ++; 
      if (logDelayTimer>150){
        logDelay=false;
        logDelayTimer=0;
      }
          }//add some latecy before the debugging stops afeter disarming;
      
        // SystemTime MS, BaroRawAlt, GPS Alt, VirtGearSmallestPlumb, Corner, Scale F_R, Scale R_R, Scale R_L, Scale F_L, Att_Pitch, Att_Roll, Z-Acc, Rangefinder, Sinkrate, Rc10, State, State_ERROR
        Serial2.printf("%d, %.2f, %d, %.2f, %d, %d, %d, %d, %d, %.4f, %.4f, %d, %.2f, %.2f, %d, %d ,%d ,%d, %d, %d\n"
                                            ,millis()//__systemTimeMs
                                            ,float(__mavlinkRawAltInt) / 10.0f /* -40.0*/// mm /10 = cm
                                            ,__mavlinkRawGpsAlt
                                            , __lowGearPlumb
                                            , __lowGearCorner
                                            ,weight3
                                            ,weight4
                                            ,weight
                                            ,weight2
                                            ,__attitudePitchRad
                                            ,__attitudeRollRad
                                            ,zacc
                                            ,__distanceSensor
                                            ,__sinkRateAverage
                                            ,Rc10
                                            ,__MAIN_STATE
                                            ,__EXT_STATE_INTERRUPT
                                            ,__stateError
                                            ,accPeak
                                            ,_bam
                                            );
      }
   }
}


//#endif
