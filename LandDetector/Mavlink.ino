void Heartbeat(){

    mavlink_message_t msg;
    uint8_t buf[MAVLINK_MAX_PACKET_LEN];
    // Initialize the required buffers

    // Pack the message //Encode a heartbeat struct into a message
    mavlink_msg_heartbeat_pack(system_id, component_id, &msg, system_type, autopilot_type, system_mode, custom_mode, system_state);

    // Copy the message to the send buffer
    uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);

    Serial1.write(buf, len);

  }


static void mavlink_test_distance_sensor(int distance, int system_id_local)
{

    if(distance > 800) distance = 0;
   
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    mavlink_status_t *status = mavlink_get_channel_status(MAVLINK_COMM_0);
    if ((status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) && MAVLINK_MSG_ID_DISTANCE_SENSOR >= 256) {
        return;
    }
#endif
    mavlink_message_t msg;
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    uint16_t i;
    mavlink_distance_sensor_t packet_in = {
        millis(),7,800,distance,163,230,MAV_SENSOR_ROTATION_PITCH_270,0,5.0,5.0,{ 0.0, 0.0, 0.0, 0.0 }
    };
    
    mavlink_distance_sensor_t packet1, packet2;
    memset(&packet1, 0, sizeof(packet1));
    
    packet1.time_boot_ms = packet_in.time_boot_ms;
    packet1.min_distance = packet_in.min_distance;
    packet1.max_distance = packet_in.max_distance;
    packet1.current_distance = packet_in.current_distance;
    packet1.type = packet_in.type;
    packet1.id = packet_in.id;
    packet1.orientation = packet_in.orientation;
    packet1.covariance = packet_in.covariance;
    packet1.horizontal_fov = packet_in.horizontal_fov;
    packet1.vertical_fov = packet_in.vertical_fov;
    
    mav_array_memcpy(packet1.quaternion, packet_in.quaternion, sizeof(float)*4);
        
#ifdef MAVLINK_STATUS_FLAG_OUT_MAVLINK1
    if (status->flags & MAVLINK_STATUS_FLAG_OUT_MAVLINK1) {
       // cope with extensions
       memset(MAVLINK_MSG_ID_DISTANCE_SENSOR_MIN_LEN + (char *)&packet1, 0, sizeof(packet1)-MAVLINK_MSG_ID_DISTANCE_SENSOR_MIN_LEN);
    }
#endif
    memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_distance_sensor_encode(system_id_local, component_id, &msg, &packet1);
    mavlink_msg_distance_sensor_decode(&msg, &packet2);
    MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

    memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_distance_sensor_pack(system_id_local, component_id, &msg , packet1.time_boot_ms , packet1.min_distance , packet1.max_distance , packet1.current_distance , packet1.type , packet1.id , packet1.orientation , packet1.covariance , packet1.horizontal_fov , packet1.vertical_fov , packet1.quaternion );
    mavlink_msg_distance_sensor_decode(&msg, &packet2);
    MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

    memset(&packet2, 0, sizeof(packet2));
    mavlink_msg_distance_sensor_pack_chan(system_id_local, component_id, MAVLINK_COMM_0, &msg , packet1.time_boot_ms , packet1.min_distance , packet1.max_distance , packet1.current_distance , packet1.type , packet1.id , packet1.orientation , packet1.covariance , packet1.horizontal_fov , packet1.vertical_fov , packet1.quaternion );
    mavlink_msg_distance_sensor_decode(&msg, &packet2);
    MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

    memset(&packet2, 0, sizeof(packet2));
    uint16_t len = mavlink_msg_to_send_buffer(buffer, &msg);

    Serial1.write(buffer, len);
}








void Mav_Request_Data()

{
  mavlink_message_t msg;
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];


  // STREAMS that can be requested
  /*
     Definitions are in common.h: enum MAV_DATA_STREAM

     MAV_DATA_STREAM_ALL=0, // Enable all data streams
     MAV_DATA_STREAM_RAW_SENSORS=1, /* Enable IMU_RAW, GPS_RAW, GPS_STATUS packets.
     MAV_DATA_STREAM_EXTENDED_STATUS=2, /* Enable GPS_STATUS, CONTROL_STATUS, AUX_STATUS
     MAV_DATA_STREAM_RC_CHANNELS=3, /* Enable RC_CHANNELS_SCALED, RC_CHANNELS_RAW, SERVO_OUTPUT_RAW
     MAV_DATA_STREAM_RAW_CONTROLLER=4, /* Enable ATTITUDE_CONTROLLER_OUTPUT, POSITION_CONTROLLER_OUTPUT, NAV_CONTROLLER_OUTPUT.
     MAV_DATA_STREAM_POSITION=6, /* Enable LOCAL_POSITION, GLOBAL_POSITION/GLOBAL_POSITION_INT messages.
     MAV_DATA_STREAM_EXTRA1=10, /* Dependent on the autopilot
     MAV_DATA_STREAM_EXTRA2=11, /* Dependent on the autopilot
     MAV_DATA_STREAM_EXTRA3=12, /* Dependent on the autopilot
     MAV_DATA_STREAM_ENUM_END=13,

     Data in PixHawk available in:
      - Battery, amperage and voltage (SYS_STATUS) in MAV_DATA_STREAM_EXTENDED_STATUS
      - Gyro info (IMU_SCALED) in MAV_DATA_STREAM_EXTRA1
  */

  // To be setup according to the needed information to be requested from the Pixhawk
  const int  maxStreams = 8;
  const uint8_t MAVStreams[maxStreams] = {
    MAV_DATA_STREAM_RAW_SENSORS, // IMU_RAW
    MAV_DATA_STREAM_EXTENDED_STATUS, //System Status, power Status, meminfo, Current_waypoint
    MAV_DATA_STREAM_RC_CHANNELS,
    MAV_DATA_STREAM_RAW_CONTROLLER,
    MAV_DATA_STREAM_POSITION,//Global Pos int ALT Baro
    MAV_DATA_STREAM_EXTRA1,  //Attidude
    MAV_DATA_STREAM_EXTRA2,  //VFR HUDh
    MAV_DATA_STREAM_EXTRA3  // Vibrations Z Rangefinder
  }; //Distance sensor etc.
  //const uint16_t MAVRates[maxStreams] =   {40, 0, 0, 0, 0, 20, 0, 20};// Updaterate for the channels above.
  //const uint16_t MAVRates[maxStreams] =   {40, 1, 1, 2, 5, 2, 1, 1};// Updaterate for the channels above.
  const uint16_t MAVRates[maxStreams] =   {40, 1, 1, 2, 5, 0, 1, 1};// Updaterate for the channels above.
  //const uint16_t MAVRates[maxStreams] =   {40, 5, 5};// Updaterate for the channels above.

  for (int i = 0; i < maxStreams; i++) {
    mavlink_msg_request_data_stream_pack(system_id, component_id, &msg, target_system, target_component, MAVStreams[i], MAVRates[i], 1);
    uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
    Serial1.write(buf, len);

  }
  
  //  // Request: PARAM_REQUEST_LIST. Only for full log recording
  //  mavlink_msg_param_request_list_pack(system_id, component_id, &msg,
  //    target_system, target_component);
  //  uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
  //  // Send
  //    Serial1.write(buf, len);

}

void comm_receive() {
  
  mavlink_message_t msg;
  mavlink_status_t status;


  while (Serial1.available() > 0) {
    digitalWrite(13,!digitalRead(13));
    uint8_t c = Serial1.read();
    if (mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &status))
    {

      // Handle message
      if(msg.sysid == 1 && msg.compid == 1){// only listen to szstem with id 1 = pixhawk
        switch (msg.msgid) {
          case MAVLINK_MSG_ID_HEARTBEAT:  // #0: Heartbeat
            { mavlink_heartbeat_t  heartbeat;
              mavlink_msg_heartbeat_decode(&msg, &heartbeat);
              int active = (mavlink_msg_heartbeat_get_base_mode(&msg)&MAV_MODE_FLAG_SAFETY_ARMED);
              //Serial.printf("System_id: %d| Component_Id %d| MODE_FLAG_SAFETY_ARMED: %d\n",msg.sysid,msg.compid, active);
              if (active == MAV_MODE_FLAG_SAFETY_ARMED && msg.sysid == 1) {// ask pixhawk on sysId 1
                armed = true;
              } else {
                armed = false;
              }
              currentmode = (uint8_t)mavlink_msg_heartbeat_get_custom_mode(&msg);

  #ifdef modeDebug
              Serial.print ("mode\t");
              Serial.println (currentmode);
  #endif

  #ifdef armDebug
              if (armed == true) {
                Serial.println("Armed");
              }

              else if (armed == false) {
                Serial.println ("Disarmed");
              }
  #endif

            }
            break;

          case MAVLINK_MSG_ID_DISTANCE_SENSOR :
            {
              //__distanceSensor = mavlink_msg_distance_sensor_get_current_distance(&msg);
  #ifdef rngfndDebug
              Serial.print("RNGFND \t"); Serial.println(DistanceSensor);
  #endif
            }
            break;

          case MAVLINK_MSG_ID_SYS_STATUS:  // #1: SYS_STATUS
            {
              mavlink_sys_status_t sys_status;
          

            }
            break;


          //        case MAVLINK_MSG_ID_PARAM_VALUE:  // #22: PARAM_VALUE
          //          {
          //            mavlink_param_value_t param_value;
          //            mavlink_msg_param_value_decode(&msg, &param_value);
          //#ifdef SOFT_SERIAL_DEBUGGING
          //            Serial.println ("PX PARAM_VAL");
          //            Serial.println(param_value.param_value);
          //            Serial.println(param_value.param_count);
          //            Serial.println(param_value.param_index);
          //            Serial.println(param_value.param_id);
          //            Serial.println(param_value.param_type);
          //            Serial.println ("Fin");
          //#endif
          //          }
          //          break;


          case MAVLINK_MSG_ID_RAW_IMU :  // #27: RAW_IMU
            {
              zacc = mavlink_msg_raw_imu_get_zacc(&msg);
              //zgyro = mavlink_msg_scaled_imu_get_zgyro(&msg);
              
              __accx = mavlink_msg_raw_imu_get_xacc(&msg);
              __accy = mavlink_msg_raw_imu_get_yacc(&msg);
              __accz = mavlink_msg_raw_imu_get_zacc(&msg);
              __gyrox = mavlink_msg_raw_imu_get_xgyro(&msg);
              __gyroy = mavlink_msg_raw_imu_get_ygyro(&msg);
              __gyroz = mavlink_msg_raw_imu_get_zgyro(&msg);

              //Serial.printf("%d, %d , %d\n",__accx,__accy,__accz);

  #ifdef debugZacc
              Serial.print("Z-Peak\t");
              Serial.println(zacc);
  #endif
            }
            break;

            /*case MAVLINK_MSG_ID_ATTITUDE:  // #30
            {
              //mavlink_attitude_t attitude;
              __attitudePitchRad = mavlink_msg_attitude_get_pitch(&msg);
              __attitudeRoll = mavlink_msg_attitude_get_roll(&msg);
            }
            break; */

          case MAVLINK_MSG_ID_GLOBAL_POSITION_INT:
            {
                __mavlinkRawAltInt = mavlink_msg_global_position_int_get_relative_alt(&msg);//int32_t mm
                __systemTimeMs = mavlink_msg_global_position_int_get_time_boot_ms(&msg);
                //Serial.println(__systemTimeMs);
            }
            break;

          case MAVLINK_MSG_ID_GPS_RAW_INT:  
            {
                __mavlinkRawGpsAlt = mavlink_msg_gps_raw_int_get_alt(&msg);//int32_t Unit = mm
                __mavlinkGPSPosLat = mavlink_msg_gps_raw_int_get_lat(&msg);
                __mavlinkGPSPosLon = mavlink_msg_gps_raw_int_get_lon(&msg);
                __mavlinkGPSTime = mavlink_msg_gps_raw_int_get_time_usec(&msg);
            }
            break;
          
  /*
          case MAVLINK_MSG_ID_VFR_HUD:
            {
              mavlink_vfr_hud_t mavlink_vfr_hud; //can be any Variable
              mavlink_msg_vfr_hud_decode(&msg, &mavlink_vfr_hud);
  #ifdef debugHeading
              Serial.print ("heading");
              Serial.println(mavlink_vfr_hund.heading); //Variable.Sensor
  #endif
          }
            break;
  */ 
  /*        case MAVLINK_MSG_ID_VIBRATION:
            {
              mavlink_vibration_t vibration;
              mavlink_msg_vibration_decode(&msg, &vibration);
  #ifdef debugVibrationZ
              Serial.print ("VibrationZ");
              Serial.println(vibration.vibration_z);
  #endif
            }
            break;
  */
          case MAVLINK_MSG_ID_RC_CHANNELS:
            {

              mavlink_rc_channels_t Rc;
              mavlink_msg_rc_channels_decode(&msg, &Rc);
              Rc10 = Rc.chan11_raw; //HAHLA RC 11

            }
            break;
  #ifdef stableTemperaturCalibration 
          case MAVLINK_MSG_ID_SCALED_PRESSURE:
            {
              temperature = mavlink_msg_scaled_pressure_get_temperature(&msg) / 100;
              //SysTime     =mavlink_msg_scaled_pressure_get_time_boot_ms(msg);
            }
            break;
  #endif
        }
      }
    }
  }


}
