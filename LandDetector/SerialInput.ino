void checkSerialInput()
{
    //Serial.printf("bool %d\n",stringComplete);
    // Check serial Input
    /*   if (stringComplete && inputString.startsWith("RANZ")) {
    Serial.println(inputString);
    inputString.remove(0,4);
    Serial.println(inputString);
    float testit = inputString.toFloat();
    Serial.print(" RANGER-Z = ");
    Serial.println(testit,2);    
    inputString = "";
    stringComplete = false;
  } */

    if (stringComplete && inputString.startsWith("E_HB"))
    {
        currentConfig.FREEFIVE = 1.0f;
        __SendHB = true;
        Serial.println("Mavlink Heartbeat on!");
        inputString = "";
        stringComplete = false;
    }

    if (stringComplete && inputString.startsWith("D_HB"))
    {
        currentConfig.FREEFIVE = 0.0f;
        __SendHB = false;
        Serial.println("Mavlink Heartbeat off!");
        inputString = "";
        stringComplete = false;
    }



    if (stringComplete && inputString.startsWith("E_RANGER_MAV_INJECT"))
    {
        currentConfig.FREESIX = 1.0f;
        __InjectRangerMav = true;
        Serial.println("Inject Rangefinder Mavlink Enabled!");
        inputString = "";
        stringComplete = false;
    }

    if (stringComplete && inputString.startsWith("D_RANGER_MAV_INJECT"))
    {
        currentConfig.FREESIX = 0.0f;
        __InjectRangerMav = false;
        Serial.println("Inject Rangefinder Mavlink Disabled!");
        inputString = "";
        stringComplete = false;
    }




    if (stringComplete && inputString.startsWith("CONSOLE_DATA_ON"))
    {
        __debugSerialTwoToConsoleOne = true;
        inputString = "";
        stringComplete = false;
    }

    if (stringComplete && inputString.startsWith("CONSOLE_DATA_OFF"))
    {
        __debugSerialTwoToConsoleOne = false;
        inputString = "";
        stringComplete = false;
    }

    if (stringComplete && inputString.startsWith("CLEAR_ERROR"))
    {
        //Serial.println(inputString);
        Serial.println("E_Fehler und Fehler IDs werden geloescht!");
        clearEepromErrorLog();
        Serial.println("E_Erledigt");
        inputString = "";
        stringComplete = false;
    }
    if (stringComplete && inputString.startsWith("DELETE_ERROR"))
    {
        //Serial.println(inputString);
        Serial.println("E_Fehler werden geloescht!");
        deleteEepromErrorLog();
        Serial.println("E_Erledigt");
        inputString = "";
        stringComplete = false;
    }

    if (stringComplete && inputString.startsWith("GET_E_HEAD"))
    {
        //Serial.println(inputString);
        Serial.println("E_ID, ERROR, STATE, GPS_TIME_USEC, GPS_LAT, GPS_LON, RANGEFINDER, MAVLINKBAROALT, MAVLINKGPSALT, SCALE_ONE, SCALE_TWO, SCALE_THREE, SCALE_FOUR, GEAR_CORNER, RC_10");

        inputString = "";
        stringComplete = false;
    }

    if (stringComplete && inputString.startsWith("GET_ERROR"))
    {
        //Serial.println(inputString);
        //Serial.println("Fehler werden geladen!");
        Serial.println("E_ID, ERROR, STATE, GPS_TIME_USEC, GPS_LAT, GPS_LON, RANGEFINDER, MAVLINKBAROALT, MAVLINKGPSALT, SCALE_ONE, SCALE_TWO, SCALE_THREE, SCALE_FOUR, GEAR_CORNER, RC_10");

        for (int i = 150; i < 2000; i = i + 50)
        {
            EEPROM.get(i, myErrors);
            if (myErrors.VALID == 112)
            {
                Serial.printf("E_%d, %d, %d, ", myErrors.ID, myErrors.ERROR, myErrors.STATE);
                print_uint64_t(myErrors.GPS_TIME);
                Serial.printf(", %d, %d, %.2f, %.2f, %d, %d, %d, %d, %d, %d, %d\n", myErrors.GPS_LAT, myErrors.GPS_LON, myErrors.RANGEFINDERPLUMB, myErrors.MAVLINKBAROALT, myErrors.MAVLINKGPSALT, myErrors.SCALEONE, myErrors.SCALETWO, myErrors.SCALETHREE, myErrors.SCALEFOUR, myErrors.CORNER, myErrors.RCTEN);
            }
        }
        inputString = "";
        stringComplete = false;
    }

    if (stringComplete && inputString.startsWith("TESTLOG"))
    {

        myErrorsTest.ID = _nextLogID;
        myErrorsTest.VALID = 112;
        myErrorsTest.ERROR = 232;
        EEPROM.put(_nextLogAddress, myErrorsTest);
        if (_nextLogAddress >= 1950)
        { // nextlog is first
            _nextLogAddress = 150;
            EEPROM.get(150, myErrorsClear);
            myErrorsClear.VALID = 0;
            EEPROM.put(150, myErrorsClear);
            //Serial.println("overrun");
        }
        else
        {
            _nextLogAddress = _nextLogAddress + 50;
            EEPROM.get(_nextLogAddress, myErrorsClear);
            myErrorsClear.VALID = 0;
            EEPROM.put(_nextLogAddress, myErrorsClear);
            //Serial.println("next-done");
        }
        _nextLogID++;
        Serial.printf("E_EEPROM DONE,_nextLogAddress: %d, _nextLogID: %d \n", _nextLogAddress, _nextLogID);

        inputString = "";
        stringComplete = false;
    }

    if(stringComplete && inputString.startsWith("HELP")){
        __debugSerialTwoToConsoleOne = false;
        Serial.println("________________________");
        Serial.println("Console buildin Commands");
        Serial.println("=====8<-----------------\n");
        Serial.println("CONSOLE_DATA_ON \t- display sensor output on console");
        Serial.println("CONSOLE_DATA_OFF \t- stop console log");
        Serial.println("CLEAR_ERROR \t\t- reset error memory and reset error ids");
        Serial.println("DELETE_ERROR \t\t- delete error memory");
        Serial.println("GET_E_HEAD \t\t- CSV Header from ERROR string");
        Serial.println("GET_ERROR \t\t- display errors");
        Serial.println("TESTLOG \t\t- create an error testlog, EEPROM test");
        Serial.println("GET_CONFIG \t\t- get Gear Configuration");
        Serial.println("SET_CONF_IMU_Z<float>\t- Set IMU Position Z");
        Serial.println("SET_CONF_RAN_X<float>\t- Set Rangefinder X Position"); 
        Serial.println("SET_CONF_RAN_Y<float>\t- Set Rangefinder Y Position"); 
        Serial.println("SET_CONF_RAN_Z<float>\t- Set Rangefinder Z Position");  
        Serial.println("SET_CONF_PON_X<float>\t- Set Gear Corner 1 X position");
        Serial.println("SET_CONF_PON_Y<float>\t- Set Gear Corner 1 Y position"); 
        Serial.println("SET_CONF_PTW_X<float>\t- Set Gear Corner 2 X position"); 
        Serial.println("SET_CONF_PTW_Y<float>\t- Set Gear Corner 2 Y position"); 
        Serial.println("SET_CONF_PTH_X<float>\t- Set Gear Corner 3 X position"); 
        Serial.println("SET_CONF_PTH_Y<float>\t- Set Gear Corner 3 Y position"); 
        Serial.println("SET_CONF_PFO_X<float>\t- Set Gear Corner 4 X position"); 
        Serial.println("SET_CONF_PFO_Y<float>\t- Set Gear Corner 4 Y position"); 
        Serial.println("SET_CONF_MGTGD<float>\t- Set Landing Distance to Ground Threshold to arm Gear Motor Off function");
        Serial.println("SET_CONF_MAA_Z<float>\t- Set Takeoff altitude to activate Gear");
        Serial.println("SET_CONF_DMSMI<int>\t- Set DSM Threshold");
        Serial.println("SET_CONF_MINAC<int>\t- Set ACC_Z THreshold");
        Serial.println("SET_CONF_SACPT<int>\t- Set ACC_Z Delay");
        Serial.println("SAVE_CONFIG\t\t- Store Configuration on internal memory");
        Serial.println("TARE_DMS\t\t- Tare Dms");
        Serial.println("SETUP_DMS\t\t- Setup Dms !This call stop the controller mainloop");
        Serial.println("E_HB\t\t\t- Enable Mavlink Heartbeat");
        Serial.println("D_HB\t\t\t- Disable Mavlink Heartbeat");
        Serial.println("E_RANGER_MAV_INJECT\t- Enable Rangefinder Mavlink injection");
        Serial.println("D_RANGER_MAV_INJECT\t- Disable Rangefinder Mavlink injection");
        
        inputString = "";
        stringComplete = false;
    }

    if (stringComplete && inputString.startsWith("SAVE_CONFIG")){
        Serial.println("Save Config on Device");
        uint32_t satrtime = micros();
        EEPROM.put(sizeof(uint16_t), currentConfig);
        Serial.printf("Save confing in %dus\n",micros() - satrtime);
        
        inputString = "";
        stringComplete = false;
    }
    

    if (stringComplete && inputString.startsWith("GET_CONFIG"))
    {
        __debugSerialTwoToConsoleOne = false;
        Serial.printf("CONFIG_%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%d,%d,%.2f,%.2f,%.2f,%.2f,%.1f,%.1f,END\n"
        ,currentConfig.IMU_Z
        ,currentConfig.RAN_X
        ,currentConfig.RAN_Y
        ,currentConfig.RAN_Z
        ,currentConfig.PONE_X
        ,currentConfig.PONE_Y
        ,currentConfig.PTWO_X
        ,currentConfig.PTWO_Y
        ,currentConfig.PTHREE_X
        ,currentConfig.PTHREE_Y
        ,currentConfig.PFOUR_X
        ,currentConfig.PFOUR_Y
        ,currentConfig.MINGEARTOGROUNDDISTANCE_Z
        ,currentConfig.MINACTIVATIONALT_Z
        ,currentConfig.DMSMINWEIGHT
        ,currentConfig.MINACC
        ,currentConfig.STOREACCPEAKTIME
        ,currentConfig.FREEONE      // DSM SCALE 1
        ,currentConfig.FREETWO      // DSM SCALE 2
        ,currentConfig.FREETHREE    // DSM SCALE 3
        ,currentConfig.FREEFOUR     // DSM SCALE 4
        ,currentConfig.FREEFIVE    // Mavlink Heartbeat
        ,currentConfig.FREESIX     // Mavlink Rangefinder        
        );
  
        inputString = "";
        stringComplete = false;    
    }


    // TARE_DMS
    if(inputString.startsWith("TARE_DMS")){// TARE DMS
        __debugSerialTwoToConsoleOne = false;
        Serial.println(inputString);
        #ifdef scaleEnable
        scales.tare(10, 100000);
        #endif
        // 3 4 1 2
        Serial.printf("DMS_1\t %d,\tDMS_2\t %d,\tDMS_3\t %d,\tDMS_4\t %d\n", weight3, weight4, weight, weight2);
        Serial.printf("Scale_1\t %.2f, Scale_2\t %.2f, Scale_3\t %.2f, Scale_4\t %.2f\n",currentConfig.FREEONE, currentConfig.FREETWO, currentConfig.FREETHREE, currentConfig.FREEFOUR);
   
        inputString = "";
        stringComplete = false; 
    }

    // TARE_DMS
    if(inputString.startsWith("SETUP_DMS")){// TARE DMS
        __debugSerialTwoToConsoleOne = false;
        // local data
        #ifdef scaleEnable

        long groundDone[5] ={0,0,0,0,0};
        long groundDtwo[5] ={0,0,0,0,0};
        long groundDthree[5] ={0,0,0,0,0};
        long groundDfour[5] ={0,0,0,0,0};

        long flightDone[5] ={0,0,0,0,0};
        long flightDtwo[5] ={0,0,0,0,0};
        long flightDthree[5] ={0,0,0,0,0};
        long flightDfour[5] ={0,0,0,0,0};
               
        
        Serial.println(inputString);
        Serial.println("Startet! Der Kopter muß nach aufforderung mehrfach angehoben und abgesetzt werden");
        Serial.println("Der Kopter steht jetzt mit Abfluggewicht auf einer ebenen und glatten Fäche.");
        delay(3000);
        for(int i = 0; i < 5; i++){
            delay(1000);
            scales.tare(10, 100000);
            Serial.printf("%d : 5 Kopter anheben\n",i+1);
            delay(3000);
            scales.read(results);

            if( CHANNEL_COUNT >= 1 ) flightDone[i]= results[0] / 10000; // 3  
            if( CHANNEL_COUNT >= 2 ) flightDtwo[i] = results[1] / 10000; // 4
            if( CHANNEL_COUNT >= 3 ) flightDthree[i] = results[2] / 10000; // 1
            if( CHANNEL_COUNT >= 4 ) flightDthree[i] = results[3] / 10000; // 2
            Serial.println("Kopter absetzen!");
            delay(3000);
            scales.read(results);
            if( CHANNEL_COUNT >= 1 ) groundDone[i] = results[0] / 10000; // 3 
            if( CHANNEL_COUNT >= 2 ) groundDtwo[i] = results[1] / 10000; // 4
            if( CHANNEL_COUNT >= 3 ) groundDthree[i] = results[2] / 10000; // 1
            if( CHANNEL_COUNT >= 4 ) groundDthree[i] = results[3] / 10000; // 2
            //Serial.printf("%d Diff Dms1: %d,Dms2 %d\n",i,flightDone[i]-groundDone[i],flightDtwo[i]-groundDtwo[i] );
        }
        // Test Data
        int maxOne = 0;
        int maxTwo = 0;
        int maxThree = 0;
        int maxFour = 0;
        int minOne = 0;
        int minTwo = 0;
        int minThree = 0;
        int minFour = 0;
        
        int FmaxOne = 0;
        int FmaxTwo = 0;
        int FmaxThree = 0;
        int FmaxFour = 0;
        int FminOne = 0;
        int FminTwo = 0;
        int FminThree = 0;
        int FminFour = 0;

        if(CHANNEL_COUNT >= 1 ) maxOne = groundDone[0];
        if(CHANNEL_COUNT >= 2 ) maxTwo = groundDtwo[0];
        if(CHANNEL_COUNT >= 3 ) maxThree = groundDthree[0];
        if(CHANNEL_COUNT >= 4 ) maxFour = groundDfour[0];

        if(CHANNEL_COUNT >= 1 ) minOne = groundDone[0];
        if(CHANNEL_COUNT >= 2 ) minTwo = groundDtwo[0];
        if(CHANNEL_COUNT >= 3 ) minThree = groundDthree[0];
        if(CHANNEL_COUNT >= 4 ) minFour = groundDfour[0];

        if(CHANNEL_COUNT >= 1 ) FmaxOne = flightDone[0];
        if(CHANNEL_COUNT >= 2 ) FmaxTwo = flightDtwo[0];
        if(CHANNEL_COUNT >= 3 ) FmaxThree = flightDthree[0];
        if(CHANNEL_COUNT >= 4 ) FmaxFour = flightDfour[0];

        if(CHANNEL_COUNT >= 1 ) FminOne = flightDone[0];
        if(CHANNEL_COUNT >= 2 ) FminTwo = flightDtwo[0];
        if(CHANNEL_COUNT >= 3 ) FminThree = flightDthree[0];
        if(CHANNEL_COUNT >= 4 ) FminFour = flightDfour[0];

        for(int i = 1; i < 5; i++){
            if(CHANNEL_COUNT >= 1 && groundDone[i] > maxOne ) maxOne = groundDone[i];
            if(CHANNEL_COUNT >= 2 && groundDtwo[i] > maxTwo ) maxTwo = groundDtwo[i];
            if(CHANNEL_COUNT >= 3 && groundDthree[i] > maxThree ) maxThree = groundDthree[i];
            if(CHANNEL_COUNT >= 4 && groundDfour[i] > maxFour ) maxFour = groundDfour[i];

            if(CHANNEL_COUNT >= 1 && groundDone[i] < minOne ) minOne = groundDone[i];
            if(CHANNEL_COUNT >= 2 && groundDtwo[i] < minTwo ) minTwo = groundDtwo[i];
            if(CHANNEL_COUNT >= 3 && groundDthree[i] < minThree ) minThree = groundDthree[i];
            if(CHANNEL_COUNT >= 4 && groundDfour[i] < minFour ) minFour = groundDfour[i];

            if(CHANNEL_COUNT >= 1 && flightDone[i] > FmaxOne ) FmaxOne = flightDone[i];
            if(CHANNEL_COUNT >= 2 && flightDtwo[i] > FmaxTwo ) FmaxTwo = flightDtwo[i];
            if(CHANNEL_COUNT >= 3 && flightDthree[i] > FmaxThree ) FmaxThree = flightDthree[i];
            if(CHANNEL_COUNT >= 4 && flightDfour[i] > FmaxFour ) FmaxFour = flightDfour[i];

            if(CHANNEL_COUNT >= 1 && flightDone[i] < FminOne ) FminOne = flightDone[i];
            if(CHANNEL_COUNT >= 2 && flightDtwo[i] < FminTwo ) FminTwo = flightDtwo[i];
            if(CHANNEL_COUNT >= 3 && flightDthree[i] < FminThree ) FminThree = flightDthree[i];
            if(CHANNEL_COUNT >= 4 && flightDfour[i] < FminFour ) FminFour = flightDfour[i];
        }
        Serial.printf("G-min: %d, %d, %d, %d| G-max: %d, %d, %d, %d\n",minOne,minTwo,minThree,minFour,maxOne,maxTwo,maxThree,maxFour);
        Serial.printf("F-min: %d, %d, %d, %d| F-max: %d, %d, %d, %d\n",FminOne,FminTwo,FminThree,FminFour,FmaxOne,FmaxTwo,FmaxThree,FmaxFour);
        
        bool pass = true;
        if(CHANNEL_COUNT >= 1 && abs(FminOne - FmaxOne) >= 50 || abs(FminOne - FmaxOne) <= 1 ) pass = false;
        if(CHANNEL_COUNT >= 2 && abs(FminTwo - FmaxTwo) >= 50 || abs(FminTwo - FmaxTwo) <= 1 ) pass = false;
        if( CHANNEL_COUNT >= 3 && (abs(FminThree - FmaxThree) >= 50 || abs(FminThree - FmaxThree) <= 1 )) pass = false;
        if( CHANNEL_COUNT >= 4 && (abs(FminFour - FmaxFour) >= 50 || abs(FminFour - FmaxFour) <= 1 )) pass = false;

        Serial.println(abs(FminOne - FmaxOne));
        Serial.println(abs(FminTwo - FmaxTwo));
        Serial.println(abs(FminThree - FmaxThree));
        Serial.println(abs(FminFour - FmaxFour));   
        Serial.println(CHANNEL_COUNT);
        
        if(pass){
            if(CHANNEL_COUNT >= 1 ){
                currentConfig.FREEONE = float(FminOne - maxOne);
                Serial.printf("DMS1: %.2f\n", currentConfig.FREEONE);
            }
            if(CHANNEL_COUNT >= 2 ){
                currentConfig.FREETWO = float(FminTwo - maxTwo);
                Serial.printf("DMS2: %.2f\n", currentConfig.FREETWO);
            }
            if(CHANNEL_COUNT >= 3 ){
                currentConfig.FREETHREE = float(FminThree - maxThree);
                Serial.printf("DMS3: %.2f\n", currentConfig.FREETHREE);
            }
            if(CHANNEL_COUNT >= 4 ){
                currentConfig.FREEFOUR = float(FminFour - maxFour);
                Serial.printf("DMS4: %.2f\n", currentConfig.FREEFOUR);
            }
            Serial.printf("Die DMS sind nun kalibriert, um die Werte dauerhaft zu übernehmen muß die Konfiguration gespeichert werden.");
            Serial.println("DMS_SETUP_DONE\n");

        }
        else
        {   
            Serial.println("Die Kalibrierung ist fehlgeschlagen!!!\n Bitte erneut probieren!");
            Serial.println("DMS_SETUP_FAIL\n"); 
        }
        #endif
        
        
        inputString = "";
        stringComplete = false; 
    }


    if (stringComplete && inputString.startsWith("SET_CONF"))
    {
        __debugSerialTwoToConsoleOne = false;
        if(inputString.startsWith("SET_CONF_IMU_Z")){// IMU_Z
            Serial.println(inputString);
            inputString.remove(0,14);
            float flstring = 0.0f;
            if(inputString.toFloat()) {
                flstring = inputString.toFloat();
                Serial.print("IMU_Z = ");
                Serial.println(flstring,2);
                currentConfig.IMU_Z = flstring;
                setLGConfigFreomSerial();
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }   
        }

        if(inputString.startsWith("SET_CONF_RAN_X")){// RANGEFINDER X
            Serial.println(inputString);
            inputString.remove(0,14);
            float flstring = 0.0f;
            if(inputString.toFloat()) {
                flstring = inputString.toFloat();
                Serial.print("RANG X = ");
                Serial.println(flstring,2);
                currentConfig.RAN_X = flstring;
                setLGConfigFreomSerial();
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }   
        }

        if(inputString.startsWith("SET_CONF_RAN_Y")){// RANGEFINDER Y
            Serial.println(inputString);
            inputString.remove(0,14);
            float flstring = 0.0f;
            if(inputString.toFloat()) {
                flstring = inputString.toFloat();
                Serial.print("RANG Y = ");
                Serial.println(flstring,2);
                currentConfig.RAN_Y = flstring;
                setLGConfigFreomSerial();
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }   
        }

        if(inputString.startsWith("SET_CONF_RAN_Z")){// RANGEFINDER Z
            Serial.println(inputString);
            inputString.remove(0,14);
            float flstring = 0.0f;
            if(inputString.toFloat()) {
                flstring = inputString.toFloat();
                Serial.print("RANG Z = ");
                Serial.println(flstring,2);
                currentConfig.RAN_Z = flstring;
                setLGConfigFreomSerial();
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }  
        }

        if(inputString.startsWith("SET_CONF_PON_X")){// LANDING GEAR Corner One X
            Serial.println(inputString);
            inputString.remove(0,14);
            float flstring = 0.0f;
            if(inputString.toFloat()) {
                flstring = inputString.toFloat();
                Serial.print("GEAR COrner One X = ");
                Serial.println(flstring,2);
                currentConfig.PONE_X = flstring;
                setLGConfigFreomSerial();
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }       
        }

        if(inputString.startsWith("SET_CONF_PON_Y")){// LANDING GEAR Corner One Y
            Serial.println(inputString);
            inputString.remove(0,14);
            float flstring = 0.0f;
            if(inputString.toFloat()) {
                flstring = inputString.toFloat();
                Serial.print("GEAR COrner One Y = ");
                Serial.println(flstring,2);
                currentConfig.PONE_Y = flstring;
                setLGConfigFreomSerial();
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }
        }

        if(inputString.startsWith("SET_CONF_PTW_X")){// LANDING GEAR Corner Two X
            Serial.println(inputString);
            inputString.remove(0,14);
            float flstring = 0.0f;
            if(inputString.toFloat()) {
                flstring = inputString.toFloat();
                Serial.print("GEAR COrner Two X = ");
                Serial.println(flstring,2);
                currentConfig.PTWO_X = flstring;
                setLGConfigFreomSerial();
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }
        }

        if(inputString.startsWith("SET_CONF_PTW_Y")){// LANDING GEAR Corner Two Y
            Serial.println(inputString);
            inputString.remove(0,14);
            float flstring = 0.0f;
            if(inputString.toFloat()) {
                flstring = inputString.toFloat();
                Serial.print("GEAR COrner Two Y = ");
                Serial.println(flstring,2);
                currentConfig.PTWO_Y = flstring;
                setLGConfigFreomSerial();
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }
        }

        if(inputString.startsWith("SET_CONF_PTH_X")){// LANDING GEAR Corner Three X
            Serial.println(inputString);
            inputString.remove(0,14);
            float flstring = 0.0f;
            if(inputString.toFloat()) {
                flstring = inputString.toFloat();
                Serial.print("GEAR COrner Three X = ");
                Serial.println(flstring,2);
                currentConfig.PTHREE_X = flstring;
                setLGConfigFreomSerial();
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }
        }

        if(inputString.startsWith("SET_CONF_PTH_Y")){// LANDING GEAR Corner Three Y
            Serial.println(inputString);
            inputString.remove(0,14);
            float flstring = 0.0f;
            if(inputString.toFloat()) {
                flstring = inputString.toFloat();
                Serial.print("GEAR COrner Three Y = ");
                Serial.println(flstring,2);
                currentConfig.PTHREE_Y = flstring;
                setLGConfigFreomSerial();
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }
        }

        if(inputString.startsWith("SET_CONF_PFO_X")){// LANDING GEAR Corner Four X
            Serial.println(inputString);
            inputString.remove(0,14);
            float flstring = 0.0f;
            if(inputString.toFloat()) {
                flstring = inputString.toFloat();
                Serial.print("GEAR Corner Four X = ");
                Serial.println(flstring,2);
                currentConfig.PFOUR_X = flstring;
                setLGConfigFreomSerial();
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }
        }

        if(inputString.startsWith("SET_CONF_PFO_Y")){// LANDING GEAR Corner Four Y
            Serial.println(inputString);
            inputString.remove(0,14);
            float flstring = 0.0f;
            if(inputString.toFloat()) {
                flstring = inputString.toFloat();
                Serial.print("GEAR Corner Four Y = ");
                Serial.println(flstring,2);
                currentConfig.PFOUR_Y = flstring;
                setLGConfigFreomSerial();
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }
        }

        if(inputString.startsWith("SET_CONF_MGTGD")){// MINGEARTOGROUNDDISTANCE_Z
            Serial.println(inputString);
            inputString.remove(0,14);
            float flstring = 0.0f;
            if(inputString.toFloat()) {
                flstring = inputString.toFloat();
                Serial.print("Landing Distanz High to Arm the Gear  = ");
                Serial.println(flstring,2);
                currentConfig.MINGEARTOGROUNDDISTANCE_Z = flstring;
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }
        }

        if(inputString.startsWith("SET_CONF_MAA_Z")){// MINACTIVATIONALT_Z
            Serial.println(inputString);
            inputString.remove(0,14);
            float flstring = 0.0f;
            if(inputString.toFloat()) {
                flstring = inputString.toFloat();
                Serial.print("Minimum Takeof Distance to activate the gear = ");
                Serial.println(flstring,2);
                currentConfig.MINACTIVATIONALT_Z = flstring;
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }
        }

        // int
        if(inputString.startsWith("SET_CONF_DMSMI")){// DSM Min Weight
            Serial.println(inputString);
            inputString.remove(0,14);
            int intstring = 0;
            if(inputString.toInt()) {
                intstring = inputString.toInt();
                Serial.print("Minimum Takeof Distance to activate the gear = ");
                Serial.println(intstring);
                currentConfig.DMSMINWEIGHT = intstring;
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }
        }

        // int
        if(inputString.startsWith("SET_CONF_MINAC")){// MIN ACC Z
            Serial.println(inputString);
            inputString.remove(0,14);
            int intstring = 0;
            if(inputString.toInt()) {
                intstring = inputString.toInt();
                Serial.print("ACCZ Threshold = ");
                Serial.println(intstring);
                currentConfig.MINACC = intstring;
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }
        }

        // int
        if(inputString.startsWith("SET_CONF_SACPT")){// MS to store High Acc Valus 
            Serial.println(inputString);
            inputString.remove(0,14);
            int intstring = 0;
            if(inputString.toInt()) {
                intstring = inputString.toInt();
                Serial.print("ACCZ Store Time = ");
                Serial.println(intstring);
                currentConfig.STOREACCPEAKTIME = intstring;
            }
            else
            {
                Serial.println("NOT VALID");
                /* code */
            }
        }            

        inputString = "";
        stringComplete = false;        
    }

    //NOTHING to do
    // clear input buffer after check
    if (stringComplete)
    {
        inputString = "";
        stringComplete = false;
    }
}

void setLGConfigFreomSerial(){
    myLandingGear.setDimensions(currentConfig.IMU_Z,
                                currentConfig.RAN_X,
                                currentConfig.RAN_Y,
                                currentConfig.RAN_Z,
                                currentConfig.PONE_X,
                                currentConfig.PONE_Y,
                                currentConfig.PTWO_X,
                                currentConfig.PTWO_Y,
                                currentConfig.PTHREE_X,
                                currentConfig.PTHREE_Y,
                                currentConfig.PFOUR_X,
                                currentConfig.PFOUR_Y);    
}

void serialEvent()
{
    while (Serial.available())
    {
        // get the new byte:
        char inChar = (char)Serial.read();
        // add it to the inputString:
        // if the incoming character is a newline, set a flag so the main loop can
        // do something about it:
        if (inChar == '\n' || inChar == ',')
        {
            stringComplete = true;
            return;
        }
        inputString += inChar;
    }
    //Serial.println(inputString);
}

void clearEepromErrorLog()
{
    _nextLogID = 1;
    for (int i = 150; i < 2000; i = i + 50)
    {
        myErrorsClear.ID = 0;
        myErrorsClear.VALID = 0;
        EEPROM.put(i, myErrorsClear);
    }
}

void deleteEepromErrorLog()
{
    for (int i = 150; i < 2000; i = i + 50)
    {
        EEPROM.get(i, myErrorsClear);
        myErrorsClear.VALID = 0;
        EEPROM.put(i, myErrorsClear);
    }
}

// helper
void print_uint64_t(uint64_t num)
{

    char rev[128];
    char *p = rev + 1;

    while (num > 0)
    {
        *p++ = '0' + (num % 10);
        num /= 10;
    }
    p--;
    /*Print the number which is now in reverse*/
    while (p > rev)
    {
        Serial.print(*p--);
    }
}
