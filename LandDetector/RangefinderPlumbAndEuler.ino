/**
 * 1. Compute Plumb distance from Mavlink Rangefinder data stream
 * 2. Rotate a Virtual Landinggear from Mavlink Attitude Pitch and Roll Data
 */

#ifdef rangeFinderPlumbAndEuler

void computePlunb(){
    // convert to degrees
    //float rollDeg = getDegree(__attitudeRollRad); 
    //float pitchDeg = getDegree(__attitudePitchRad);

    // allready degrees Change in computeAHRS()
    float rollDeg = __attitudeRollDeg; 
    float pitchDeg = __attitudePitchDeg;
    
    __attitudeAngleDegSum = sqrt((pitchDeg*pitchDeg) + (rollDeg*rollDeg));
    __rangefinderPlumb = cos(__attitudeAngleDegSum * M_PI/180.0f) * float(__distanceSensor);


    //sinkrate calculation
    uint32_t timediff = millis() - __oldSinkrateTime;
    __oldSinkrateTime = millis();

    __sinkRate = (__rangefinderPlumb - __oldSinkRatePlumb) / float(timediff) * 1000.0f;

    //if(abs(__sinkRate) >= 500.0) Serial.printf("V-Speed > 100 cm/sek. Sinkrate: %.2f\n", __sinkRate);

    __oldSinkRatePlumb = __rangefinderPlumb;
    __sinkRateAverage = 0.9f*__sinkRateAverage + 0.1f*__sinkRate;

    //Serial.printf("TimeDiff: %d, Sinkrate: %.3f, Plumb: %.3f\n",timediff,__sinkRate,__rangefinderPlumb);
    //Serial.printf("Sinkrate: %.3f, SinkAVR: %.3f\n",__sinkRate,__sinkRateAverage);
}

void computeVirtualLandingGear(){
    myLandingGear.solveEulerRotation(__attitudeRollRad*-1.0f,__attitudePitchRad*1.0f, 0.0f);
    __lowGearPlumb = __rangefinderPlumb + myLandingGear.getLowestGearLevel();
    __lowGearCorner = myLandingGear.getLowestCorner();

    #ifdef  debugRangeFinderPlumbAndEuler
    Serial.printf("Mav_Pitch: %.2f || Mav_Roll: %.2f || AngleSum: %.2f°Deg || GearPlumb %.2f || GearCorner %d || RangeFinderRaw %.2f\n",
                  __attitudePitchRad, __attitudeRollRad, __attitudeAngleDegSum, __lowGearPlumb, __lowGearCorner, float(__distanceSensor));
    #endif
}


void computeAHRS(){


  float ax, ay, az;
  float gx, gy, gz;
  float rollrad, pitchrad, headingrad, rolldeg, pitchdeg, headingdeg;

  // check if it's time to read data and update the filter

    // read raw data from CurieIMU
    //CurieIMU.readMotionSensor(aix, aiy, aiz, gix, giy, giz);

    // convert from raw data to gravity and degrees/second units
    ax = convertRawAcceleration(1*__accx);
    ay = convertRawAcceleration(-1*__accy);
    az = convertRawAcceleration(-1*__accz);
    gx = convertRawGyro(__gyrox);
    gy = convertRawGyro(-1*__gyroy);
    gz = convertRawGyro(__gyroz);

    // update the filter, which computes orientation
    filterAHRS.updateIMU(gx, gy, gz, ax, ay, az);

    // print the heading, pitch and roll
    rollrad = filterAHRS.getRollRadians();
    pitchrad = filterAHRS.getPitchRadians();
    headingrad = filterAHRS.getYawRadians();
    rolldeg = filterAHRS.getRoll();
    pitchdeg = filterAHRS.getPitch();
    headingdeg = filterAHRS.getYaw();
    __attitudeRollRad = rollrad;
    __attitudePitchRad = pitchrad;
    __attitudeRollDeg = rolldeg;
    __attitudePitchDeg = pitchdeg;    


    // Serial.printf("Pitch: %.2fDeg, Roll %.2fDeg, Yaw %.2fDeg: Pitch: %.3fRad, Roll %.3fRad, Yaw %.3fRad\n",
    //                                                             filterAHRS.getPitch(),
    //                                                             filterAHRS.getRoll(),
    //                                                             filterAHRS.getYaw(),
    //                                                             filterAHRS.getPitchRadians(),
    //                                                             filterAHRS.getRollRadians(),
    //                                                             filterAHRS.getYawRadians()); 

}


//Helper
float getDegree(float radi){
  return radi * (180.0/M_PI);
}

float convertRawAcceleration(int aRaw) {
  // since we are using 2G range
  // -2g maps to a raw value of -32768
  // +2g maps to a raw value of 32767

  float a = (float(aRaw) * 16.0) / 32768.0;
  return a;
}

float convertRawGyro(int gRaw) {
  // since we are using 250 degrees/seconds range
  // -250 maps to a raw value of -32768
  // +250 maps to a raw value of 32767

  float g = (float(gRaw) * 4000.0) / 32768.0;
  return g;
}

#endif
