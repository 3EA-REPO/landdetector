bool arming (bool runMotors){
  //Command Long
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  uint16_t command=MAV_CMD_COMPONENT_ARM_DISARM; //<  Command ID (of command to send)
  uint8_t target_component; //  Component which should execute the command, 0 for all components
  uint8_t confirmation; //  0: First transmission of this command. 1-255: Confirmation transmissions (e.g. for kill command)

  mavlink_message_t msg;

  if (runMotors==false&&armed==true&&1==1){
    mavlink_msg_command_long_pack(system_id,component_id,&msg,target_system,target_component,MAV_CMD_COMPONENT_ARM_DISARM,confirmation,0,21196,0,0,0,0,0);}//Serial.println ("Disarm Command");}//Disarming
    //mavlink_msg_command_long_pack(system_id,component_id,&msg,target_system,target_component,command,confirmation,param1,param2,param3,param4,param5,param6,param7);

  uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
  // Send the message
  Serial1.write(buf, len);
   
 }



#ifdef changeModeEnable
 bool changeMode(int ModeNumber){

if (currentmode!=ModeNumber){   
    custom_mode=ModeNumber;
    mavlink_message_t msg;
    uint8_t buf[MAVLINK_MAX_PACKET_LEN];
    mavlink_msg_set_mode_pack( system_id, component_id, &msg,target_system,base_mode, custom_mode);
    uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
    Serial1.write(buf, len);

    //Serial.println  ("Message changeMode");
 
    
    return 0;

  }
 }
#endif //changeModeEnable 

#ifdef setParamEnable
bool setParam(word param, int val){
    mavlink_message_t msg;
    uint8_t buf[MAVLINK_MAX_PACKET_LEN];  
    //strcpy (param_id,"RTL_ALT");
    strcpy (param_id,(param));
    param_value=val;
    param_type=0;
    
    mavlink_msg_param_set_pack( system_id, component_id,&msg,target_system,target_component,param_id,param_value,param_type);//
    //////1500.00 // parameter value
    ////818       //param_count
    ////23        //param_id
    ////RTL_ALT   //param_name
    ////4         //param_type
    uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
    // Send the message
     Serial1.write(buf, len);
     # ifdef paramDeburg
     Serial.println ("Message Param/t");Serial.print(param);
     #endif  
   return 0;
     }
#endif //setParamEnable

void storeAccPeak(){
  
  #ifdef useFireFilterDeltaAccX
  int accDiff = fir.processReading(abs(zacc))-abs(zacc);
  //if(accDiff>100) Serial.printf("accdiff: %d, raw: %d, accPeak: %d\n",accDiff,zacc,accPeak);
  if(accDiff<-500){
    storeBamTime = millis();
    _bam = true;

  }
  if( _bam == true && millis() - storeBamTime > 300 ){
    _bam=false;
    } 
  #endif

  if( zacc < currentConfig.MINACC ){
    storeAccTime = millis();
    accPeak = true;
    }
  if( accPeak == true && millis() - storeAccTime > currentConfig.STOREACCPEAKTIME ){
    accPeak=false;
    }
}

